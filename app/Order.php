<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'order_date',
        'order_price',
    ];

    public function orderLines()
    {
        return $this->hasMany(OrderLine::class);
    }
}
