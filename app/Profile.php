<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'profile_first_name',
        'profile_last_name',
        'profile_address1',
        'profile_address2',
        'profile_postal_code',
        'profile_city',
        'profile_gsm',
        'profile_telephone',
    ];
}
