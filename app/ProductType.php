<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = [
        'product_type_name',
        'product_type_body',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
