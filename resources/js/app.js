import Vue from 'vue'
import Router from "vue-router";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('navigation', require('./components/Navigation.vue').default);
Vue.component('product-list', require('./components/ProductList.vue').default);
Vue.component('shopping-cart', require('./components/ShoppingCart.vue').default);
Vue.component('app', require('./App.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(Router);

import App from "./App.vue"
import Home from "./views/Home"
import Recipes from "./views/Recipes";
import Questions from "./views/Questions";
import Reviews from "./views/Reviews";
import Webshop from "./views/Webshop";
import ShowProduct from "./views/ShowProduct";
import OrderDetails from "./views/OrderDetails";
import OrderConfirmation from "./views/OrderConfirmation";
import AdminHome from "./views/admin/Home"

import AdminOrders from "./views/admin/orders/AdminOrders";
import AdminViewOrder from "./views/admin/orders/AdminViewOrder";
import AdminMakeOrder from "./views/admin/orders/AdminMakeOrder";
import AdminEditOrder from "./views/admin/orders/AdminEditOrder";

import AdminCustomers from "./views/admin/customers/AdminCustomers";
import AdminViewCustomer from "./views/admin/customers/AdminViewCustomer";
import AdminMakeCustomer from "./views/admin/customers/AdminMakeCustomer";
import AdminEditCustomer from "./views/admin/customers/AdminEditCustomer";

import AdminProducts from "./views/admin/products/AdminProducts";
import AdminViewProduct from "./views/admin/products/AdminViewProduct";
import AdminMakeProduct from "./views/admin/products/AdminMakeProduct";
import AdminEditProduct from "./views/admin/products/AdminEditProduct";

import AdminProductTypes from "./views/admin/productTypes/AdminProductTypes";
import AdminViewProductType from "./views/admin/productTypes/AdminViewProductType";
import AdminMakeProductType from "./views/admin/productTypes/AdminMakeProductType";
import AdminEditProductType from "./views/admin/productTypes/AdminEditProductType";


const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "Home",
            component: Home,
        },

        {
            path: "/recipes",
            name: "Recipes",
            component: Recipes,
        },

        {
            path: "/questions",
            name: "Questions",
            component: Questions,
        },

        {
            path: "/reviews",
            name: "Reviews",
            component: Reviews,
        },

        {
            path: "/webshop",
            name: "Webshop",
            component: Webshop,
        },

        {
            path: "/webshop/producttype/:productTypeId",
            name: "ShowProductType",
            component: Webshop,
        },

        {
            path: "/webshop/product/:productId",
            name: "ShowProduct",
            component: ShowProduct,
        },

        {
            path: "/orderDetails",
            name: "OrderDetails",
            component: OrderDetails,
        },

        {
            path: "/orderConfirmation",
            name: "OrderConfirmation",
            component: OrderConfirmation,
        },

        {
            path: "/admin",
            name: "AdminHome",
            component: AdminHome,
        },

        {
            path: "/admin/orders",
            name: "AdminOrders",
            component: AdminOrders,
        },

        {
            path: "/admin/makeOrder",
            name: "AdminMakeOrder",
            component: AdminMakeOrder,
        },

        {
            path: "/admin/viewOrder/:orderId",
            name: "AdminViewOrder",
            component: AdminViewOrder,
        },

        {
            path: "/admin/editOrder/:orderId",
            name: "AdminEditOrder",
            component: AdminEditOrder,
        },

        {
            path: "/admin/customers",
            name: "AdminCustomers",
            component: AdminCustomers,
        },

        {
            path: "/admin/makeCustomer",
            name: "AdminMakeCustomer",
            component: AdminMakeCustomer,
        },

        {
            path: "/admin/viewCustomer/:customerId",
            name: "AdminViewCustomer",
            component: AdminViewCustomer,
        },

        {
            path: "/admin/editCustomer/:customerId",
            name: "AdminEditCustomer",
            component: AdminEditCustomer,
        },

        {
            path: "/admin/products",
            name: "AdminProducts",
            component: AdminProducts,
        },

        {
            path: "/admin/makeProduct",
            name: "AdminMakeProduct",
            component: AdminMakeProduct,
        },

        {
            path: "/admin/viewProduct/:productId",
            name: "AdminViewProduct",
            component: AdminViewProduct,
        },

        {
            path: "/admin/editProduct/:productId",
            name: "AdminEditProduct",
            component: AdminEditProduct,
        },

        {
            path: "/admin/productTypes",
            name: "AdminProductTypes",
            component: AdminProductTypes,
        },

        {
            path: "/admin/makeProductType",
            name: "AdminMakeProductType",
            component: AdminMakeProductType,
        },

        {
            path: "/admin/viewProductType/:productTypeId",
            name: "AdminViewProductType",
            component: AdminViewProductType,
        },

        {
            path: "/admin/editProductType/:productTypeId",
            name: "AdminEditProductType",
            component: AdminEditProductType,
        },
    ]
})

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
