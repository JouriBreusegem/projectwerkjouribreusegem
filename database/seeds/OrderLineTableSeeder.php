<?php

use Illuminate\Database\Seeder;
use App\OrderLine;

class OrderLineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OrderLine::class, 200)->create();
    }
}
