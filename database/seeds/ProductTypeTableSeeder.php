<?php

use Illuminate\Database\Seeder;
use App\ProductType;

class ProductTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ProductType::class, 5)->create();
    }
}
