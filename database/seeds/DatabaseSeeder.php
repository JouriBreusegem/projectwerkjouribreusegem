<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(ProductTypeTableSeeder::class);

        $this->call(ProductTableSeeder::class);
        $this->call(OrderTableSeeder::class);

        $this->call(OrderLineTableSeeder::class);

        Model::reguard();
    }
}
