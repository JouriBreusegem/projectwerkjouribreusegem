<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\ProductType;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    $productTypes = ProductType::pluck('id')->toArray();

    return [
        'product_type_id' => $faker->randomElement($productTypes),
        'product_name' => $faker->word,
        'product_price' => $faker->numberBetween($min = 0, $max = 50),
        'product_body' => $faker->text,
    ];
});
