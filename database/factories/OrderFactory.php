<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    $users = User::pluck('id')->toArray();

    return [
        'user_id' => $faker->randomElement($users),
        'order_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'order_price' => $faker->numberBetween($min = 0, $max = 500),
    ];
});
