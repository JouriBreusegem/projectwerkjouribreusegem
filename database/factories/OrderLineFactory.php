<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\OrderLine;
use App\Order;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(OrderLine::class, function (Faker $faker) {
    $products = Product::pluck('id')->toArray();
    $orders = Order::pluck('id')->toArray();

    return [
        'order_id' => $faker->randomElement($orders),
        'product_id' => $faker->randomElement($products),
        'order_line_quantity' => $faker->numberBetween($min = 0, $max = 15),
        'order_line_price' => $faker->numberBetween($min = 0, $max = 500),
    ];
});
